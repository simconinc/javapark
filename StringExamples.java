/* String Examples Program */
class StringExamples  {
  public static void main(String[] args) {
    System.out.println("Examples of String object equalities");
    String jag = new String("Black Jaguar");
    String zag; 
    zag = jag; 
    System.out.println("jag = " + jag + " zag = " + zag);
    if(zag == jag) {
      System.out.println("jag is == zag since they are the same object in memory.");
    }
    String kag = new String(); 
    kag = jag;
    System.out.println("jag = " + jag + " kag = " + kag);
    if(kag == jag) {
      System.out.println("jag is == kag since they are the same object in memory.");
          }
     else
     {
      System.out.println("jag is != kag");
     }
    String wag = new String("Black Jaguar");
    System.out.println("jag = " + jag + " wag = " + wag);
    if(wag == jag) {
      System.out.println("jag is == wag");
     }
     else
     {
      System.out.println("jag is != wag becuase they are different objects in memory.");
     }
    if (jag.equals(wag)) {
      System.out.println("But jag.equals(wag) is true because while jag and wag are two different objects in memory, the string wrapped by jag is equal to the string wrapped by wag.");
      System.out.println("The End");
     }
   } 
 }